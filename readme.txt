1. Project TryCatch

- core class are loaded by vendor autoload
- custom class from application loaded by class what i write
- created like framework MVC
- Framework can work like RESTful or normal site html only need extract Api or default Controller Abstract class
- folder library - core function
- we have two class in folder library for controller:
    - ApiControllerAbstract for API
    - ControllerAbstract for normal html site
- all code is write use coding standard PSR2
- all code is tested by PHP Code Sniffer standard PSR2
- all code is tested by PHP Mess Detector www.phpmd.org
- I added all the documentation in PHPDocumentor in folder docs/trycatch
- I prepared 10 test API - you find this in tests/api/ folder
- In tests/_output you find all "report.html" for this test and in folder "api.remote.coverage" all code coverage

If you want run test you must:
- in public .htaccess change index.php to index-test.php
 run function from root project: codecept run api --steps - show you step by step all test

phpdoc.xml - configuration for PHPDocumentor
codeception.yml - configuration for tests
composer.phar - install,update vendor class
composer.json - configuration file for composer