<?php
/**
 * Model for users implements ModelInterface CRUD
 */

use Pto\Model\ModelInterface;
use Pto\Files\FileCsv;
use Pto\Exception\ApiException;
use Pto\Main;

/**
 * CRUD model of Create, Read, Update, Delete on file *.csv
 * Class Application_Model_Users
 * @package Api\Models\Users
 */
class Application_Model_Users implements ModelInterface
{
    /**
     * handler FileCsv
     * @var FileCsv
     */
    public $file;

    /**
     * add $file FileCsv
     * @param FileCsv $file
     */
    public function __construct(FileCsv $file)
    {
        $this->file = $file;
        $this->file->setRequiredKeys(array('name','phone','street'));
    }

    /**
     * Function add new item to file
     * @param array $data
     * @return bool
     */
    public function create(array $data)
    {
        $item = $this->file->addData($data);
        return $item;
    }

    /**
     * Function get item by id all return all items
     * @param $dataId
     * @uses Main::application()->getResponse()->setResponseCode() to setResponseCode
     * @return array|bool
     * @throws ApiException
     */
    public function read($dataId)
    {
        if ($dataId>0) {
            $data = $this->file->getDataById($dataId);
        } else {
            $data = $this->file->getAllData();
        }

        if ($data===false) {
            Main::application()->getResponse()->setResponseCode('404');
            throw new ApiException('Data not exist', ApiException::DATA_NOT_EXIST);
        }

        return $data;
    }

    /**
     * Function change item by id
     * @param int $dataId
     * @param array $data
     * @return mixed
     */
    public function update($dataId, array $data)
    {
        return $this->file->updateData($dataId, $data);
    }

    /**
     * Function delete item by id
     * @param int $dataId
     * @return boolean
     */
    public function delete($dataId)
    {
        return $this->file->removeKey($dataId);
    }
}
