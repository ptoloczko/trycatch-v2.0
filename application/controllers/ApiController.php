<?php
/**
 * used for api
 */

use Pto\Controller\ApiControllerAbstract;
use Pto\Model;
use Pto\Main;
use Pto\Validate;

/**
 * Class ApiController
 * @package Api\Controller\ApiController
 */
class ApiController extends ApiControllerAbstract
{
    /**
     * Method get item from $_GET
     * @see \Pto\Validate::validateInt() to validate integer data
     * @throws \Pto\Exception\ApiException
     */
    public function index()
    {
        $dataId = $this->getRequest()->getParam('id');

        if ($dataId!==null) {
            $dataId = Validate::validateInt($this->getRequest()->getParam('id'), 'id');
        }

        $file = new \Pto\Files\FileCsv(Main::getOption('csv_file_name'));
        $model = new Application_Model_Users($file);
        $item = $model->read($dataId);
        $this->view->body = array('errors' => false, 'items' => $item);
    }

    /**
     * Method create new item from $_POST
     * @uses \Pto\Validate::validateString() to validate string data
     * @throws \Pto\Exception\ApiException
     */
    public function post()
    {
        $name   = Validate::validateString($this->getRequest()->getParam('name'), 'name');
        $street = Validate::validateString($this->getRequest()->getParam('street'), 'street');
        $phone  = Validate::validateString($this->getRequest()->getParam('phone'), 'phone');

        $data = array('name' => $name,
            'street' => $street,
            'phone' => $phone);

        $file = new \Pto\Files\FileCsv(Main::getOption('csv_file_name'));
        $model = new Application_Model_Users($file);

        if ($name=='' || $street=='' || $phone=='') {
            throw new \Pto\Exception\ApiException('Error edit data', \Pto\Exception\ApiException::ERROR_WRONG_PARAM);
        }

        if (!($item = $model->create($data))) {
            throw new \Pto\Exception\ApiException('Error edit data', \Pto\Exception\ApiException::ERROR_WRONG_PARAM);
        }

        $this->view->body = array('errors' => false, 'items' => $item, 'message'=> 'Created');
    }

    /**
     * function not used now, redirect to method "index"
     */
    public function get()
    {
        //if get default is set to index method
    }

    /**
     * Method used to DELETE item
     * @uses \Pto\Validate::validateString() to validate string data
     * @throws \Pto\Exception\ApiException
     */
    public function delete()
    {
        $dataId = Validate::validateString($this->getRequest()->getParam('id'), 'id');
        $file = new \Pto\Files\FileCsv(Main::getOption('csv_file_name'));
        $model = new Application_Model_Users($file);

        if (!$model->delete($dataId)) {
            throw new \Pto\Exception\ApiException('Error edit data', \Pto\Exception\ApiException::ERROR_WRONG_PARAM);
        }

        $this->view->body = array('errors' => false, 'message' => 'delete ok');
    }

    /**
     * Method used to edit item
     */
    public function put()
    {
        $dataId = Validate::validateInt($this->getRequest()->getParam('id'), 'id');
        $name   = Validate::validateString($this->getRequest()->getParam('name'), 'name');
        $street = Validate::validateString($this->getRequest()->getParam('street'), 'street');
        $phone  = Validate::validateString($this->getRequest()->getParam('phone'), 'phone');

        $file = new \Pto\Files\FileCsv(Main::getOption('csv_file_name'));


        if ($name=='' || $street=='' || $phone=='') {
            throw new \Pto\Exception\ApiException('Error edit data', \Pto\Exception\ApiException::ERROR_WRONG_PARAM);
        }

        $data = array('name' => $name,
            'street' => $street,
            'phone' => $phone);

        $model = new Application_Model_Users($file);
        if (!$model->update($dataId, $data)) {
            throw new \Pto\Exception\ApiException('Error edit data', \Pto\Exception\ApiException::ERROR_WRONG_PARAM);
        }
        $this->view->body = array('errors' => false, 'message' => 'update ok');
    }
}
