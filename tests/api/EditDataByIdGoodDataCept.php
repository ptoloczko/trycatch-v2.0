<?php 
$I = new ApiTester($scenario);
$I->wantTo('edit data by id=1');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendPUT('/', ['name' => 'New name', 'phone'=>'1234', 'street'=>'New street', 'id'=> 1]);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => false));
