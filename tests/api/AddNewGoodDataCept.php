<?php 
$I = new ApiTester($scenario);
$I->wantTo('add good data');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendPOST('/', ['name'=> 'good data', 'phone'=>'1111', 'street'=>'good street']);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => false));
