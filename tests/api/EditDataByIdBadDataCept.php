<?php 
$I = new ApiTester($scenario);
$I->wantTo('edit data wrong elements');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendPUT('/', ['id'=> 1]);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => true));
