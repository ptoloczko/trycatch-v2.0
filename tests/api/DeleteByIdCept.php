<?php

$I = new ApiTester($scenario);
$I->wantTo('delete by good id=1');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendDELETE('/', ['id'=> 1]);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => false));
$I->seeResponseContainsJson(array('message'=> 'delete ok'));
