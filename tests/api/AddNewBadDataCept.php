<?php 
$I = new ApiTester($scenario);
$I->wantTo('add new bad data');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendPOST('/', ['name'=>'some name']);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => true));

