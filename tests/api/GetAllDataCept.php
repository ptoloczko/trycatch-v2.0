<?php

$I = new ApiTester($scenario);
$I->wantTo('get all data from api');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendGET('/');
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => false));
