<?php

$I = new ApiTester($scenario);
$I->wantTo('get data by id not integer');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendGET('/', ['id' => 'jakiś string']);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => true));
