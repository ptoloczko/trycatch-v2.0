<?php 
$I = new ApiTester($scenario);
$I->wantTo('delete by wrong id');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendDELETE('/', ['id'=> 100]);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => true));
