<?php 
$I = new ApiTester($scenario);
$I->wantTo('get Data by Id=1');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendGET('/', ['id' => 1]);
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(array('errors' => false));

