<?php

$I = new ApiTester($scenario);
$I->wantTo('get data by id=100');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendGET('/', ['id' => 100]);
$I->seeResponseCodeIs(404);
$I->seeResponseContainsJson(array('errors' => true));
