<?php

if (isset($_SERVER["REQUEST_URI"]) && !empty($_SERVER["REQUEST_URI"])) {
    $_SERVER["REQUEST_URI"] = str_replace('index.php', '', $_SERVER["REQUEST_URI"]);
}

// Define path root directory
defined('APPLICATION_ROOT')
|| define('APPLICATION_ROOT', realpath(dirname(__FILE__) . '/../'));

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

define('DS', DIRECTORY_SEPARATOR);

//load vendor autoload
if(!is_file(__DIR__ . '/../vendor/autoload.php')){
    die('You need run "php composer.phar install" from root directory');
}
require_once __DIR__ . '/../vendor/autoload.php';


switch (APPLICATION_ENV) {
    case 'development':
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        break;
    case 'production':
        error_reporting(0);
        break;
    default:
        exit('Set application development in htaccess');
}

// Create and run main application
Pto\Main::run(APPLICATION_ENV, 'ini', APPLICATION_ROOT . DS .'configs'. DS .'config.ini');