<?php
/**
 * Class use to configuration
 */

namespace Pto;

use Pto\Configuration\Driver\Ini;

/**
 * Class Configuration
 * @package Pto
 */
class Configuration
{
    /**
     * @const string 'ini'
     */
    const TYPE_INI = 'ini';
    /**
     * @const string 'php'
     */
    const TYPE_PHP = 'php';
    /**
     * @const string 'xml'
     */
    const TYPE_XML = 'xml';

    /**
     * store $options
     * @var array
     */
    private $options;

    /**
     * @var string type of file extension
     */
    private $type;

    /**
     * setup type of file
     * @param string $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * set type of file
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * load configuration from file
     * @param $fileName
     * @return array
     * @throws \Exception
     */
    public function loadConfiguration($fileName)
    {
        if (!$this->type) {
            throw new \Exception('Invalid type');
        }

        switch (strtolower($this->type)) {
            case Configuration::TYPE_INI:
                $config = new Ini($fileName);
                break;
            default:
                throw new \EXception('Invalid type');
                break;
        }

        return $this->options = $config->getData();
    }
}
