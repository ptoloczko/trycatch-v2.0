<?php
/**
 * Use for response to client
 */

namespace Pto;

/**
 * Class Response
 * @package Pto
 */
class Response
{

    /**
     * @const string default Content-Type
     */
    const CONTENT_TYPE = 'Content-Type';

    /**
     * @const string default text/html
     */
    const CONTENT_TYPE_DEFAULT = 'text/html';

    /**
     * save return code
     * @var int
     */
    protected $code = 200;

    /**
     * set headers
     * @var array
     */
    protected $headers = array(self::CONTENT_TYPE=>self::CONTENT_TYPE_DEFAULT);

    /**
     * array of response code
     * @var array
     */
    private $responseStatusCode = array(
        '200' => 'OK',
        '201' => 'CREATED',
        '404' => 'Not Found',
        '500' => 'Internal Server Error'
    );

    /**
     * setup response code
     * @param int $code
     * @return $this
     */
    public function setResponseCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * setup header
     * @param $name
     * @param $value
     * @return $this
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * send headers
     */
    public function sendHeaders()
    {
        foreach ($this->headers as $key => $value) {
            header($key.': '.$value);
        }
    }

    /**
     * send response
     */
    public function sendResponse()
    {
        if (isset($this->responseStatusCode[$this->code])) {
            header("HTTP/1.1 " . $this->code . " " . $this->responseStatusCode[$this->code]);
        }
        $this->sendHeaders();
    }

    /**
     * setup Content-Type
     * @param $contentType
     */
    public function setContentType($contentType)
    {
         $this->setHeader(self::CONTENT_TYPE, $contentType);
    }
}
