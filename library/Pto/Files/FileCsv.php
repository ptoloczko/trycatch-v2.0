<?php
/**
 * Use to manipulate csv file store,edit,save,get
 */

namespace Pto\Files;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class FileCsv
 * @package Pto\Files
 */
class FileCsv extends FileAbstract
{
    /**
     * save all $rows from file
     * @var array
     */
    protected $addresses = [];

    /**
     * setup required fields to store element
     * @var array
     */
    protected $requiredKeys = array('name', 'phone', 'street');

    /**
     * filter to checking file extension
     * @var array
     */
    protected $filter = ['extension' => 'csv'];

    /**
     * delimiter
     * @var string
     */
    protected $delimiter = ',';

    /**
     * read all rows from file
     * @param null $fileName
     */
    public function __construct($fileName = null)
    {
        if ($fileName !== null) {
            $this->addFile($fileName);
        } else {
            if (!$this->checkIfFileExist()) {
                throw new Exception('File not exist');
            }
        }

        $this->validFilter();

        if (!$this->isValid()) {
            throw new Exception('File filter is not valid');
        }

        $this->readCsvData();
        return $this;
    }

    /**
     * return required keys
     * @return array
     */
    public function getRequiredKeys()
    {
        return $this->requiredKeys;
    }

    /**
     * setup required keys
     * @param array $requiredKeys
     */
    public function setRequiredKeys($requiredKeys)
    {
        if (is_array($requiredKeys)) {
            $this->requiredKeys = $requiredKeys;
        }
    }

    /**
     * get data row form file
     * @param $dataId
     * @return mixed
     */
    public function getDataById($dataId)
    {
        return $this->keyDataExist($dataId);
    }

    /**
     * get all rows from file
     * @return array
     */
    public function getAllData()
    {
        return $this->addresses;
    }


    /**
     * Function update data by id
     * @param $dataId
     * @param array $data
     * @return bool true/false
     * @internal param int $id
     */
    public function updateData($dataId, array $data)
    {
        if (!$this->checkData($data)) {
            return false;
        }

        if (!isset($this->addresses[$dataId])) {
            return false;
        }

        $newData = $this->prepareData($data, true);
        $newData = array_merge($this->addresses[$dataId], $newData);
        $this->addresses[$dataId] = $newData;
        $this->writeCsvData($this->addresses);
        return true;
    }

    /**
     * store rows to $addresses from file
     */
    protected function readCsvData()
    {
        if (!$this->checkIfFileExist()) {
            throw new Exception("File " . self::$fileName . " not exist");
        }

        $file = fopen(self::$fileName, 'r');
        while (($line = fgetcsv($file, null, $this->delimiter)) !== false) {
            list($name, $phone, $street) = $line;
            $this->addresses[] = [
                'name' => $name,
                'phone' => $phone,
                'street' => $street
            ];
        }
        fclose($file);
    }


    /**
     * write all rows to file
     * @param array $data
     */
    protected function writeCsvData(array $data)
    {

        if (!$this->checkIfFileExist()) {
            throw new Exception('File not exist');
        }

        $file = fopen(self::$fileName, 'w');
        $data = (count($data) > 0) ? $data : $this->addresses;
        foreach ($data as $value) {
            fputcsv($file, $value, $this->delimiter);
        }
        fclose($file);
    }

    /**
     * add new element to $this->_addresses array and store new changes to file
     * @param $data
     * @return bool true/false
     */
    public function addData($data)
    {

        if (!$this->checkData($data)) {
            return false;
        }

        $insertData = $this->prepareData($data);

        array_push($this->addresses, $insertData);
        $this->writeCsvData($this->addresses);

        return true;
    }


    /**
     * Function prepare in good order data
     * @param array $data
     * @param boolean $removeEmptyKey true - remove from array empty value
     * @return array
     */
    private function prepareData(array $data, $removeEmptyKey = false)
    {
        $goodData = array();
        foreach ($this->requiredKeys as $key) {
            if ($removeEmptyKey === true) {
                if ($data[$key]=='') {
                    continue;
                }
            }
            $goodData[$key] = $data[$key];
        }
        return $goodData;
    }

    /**
     * Function check if array of data has required keys
     * @param array $data
     * @return bool true/false
     */
    public function checkData(array $data)
    {
        $tmpData = array();
        foreach ($this->requiredKeys as $key) {
            if (isset($data[$key])) {
                $tmpData[$key] = $data[$key];
            } else {
                return false;
            }
        }
        return true;
    }


    /**
     * Function to remove elements from array $this->_addresses and write change to file
     * @param $key
     * @return bool
     */
    public function removeKey($key)
    {
        if (!$this->keyDataExist($key)) {
            return false;
        }

        unset($this->addresses[$key]);
        $this->writeCsvData($this->addresses);
        return true;
    }

    /**
     * set delimiter
     * @param $delimiter
     * @return $this;
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
        return $this;
    }

    /**
     * check if $key of row exist
     * @param $key
     * @return bool
     */
    private function keyDataExist($key)
    {
        if (!isset($this->addresses[$key])) {
            return false;
        }
        return $this->addresses[$key];
    }
}
