<?php
/**
 * Abstract File
 */

namespace Pto\Files;

/**
 * Class FileAbstract
 * @package Pto\Files
 */
abstract class FileAbstract
{
    /**
     * name of file
     * @var string
     */
    protected static $fileName;

    /**
     * check if file is valid
     * @var bool
     */
    protected $isValid = true;

    /**
     * filters [$key=>$value]
     * @var array
     */
    protected $filter;

    /**
     * check if file exist
     * @param null $fileName
     * @return bool true/false
     */
    public static function checkIfFileExist($fileName = null)
    {
        if ($fileName === null) {
            $fileName = self::$fileName;
        }
        if (is_writable($fileName)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * getExtension of file
     * @return string
     */
    public function getExtension()
    {
        $pathParts = pathinfo(self::$fileName);
        if (isset($pathParts["extension"])) {
            return $pathParts["extension"];
        } else {
            return '';
        }
    }

    /**
     * set filter
     * @param string $name
     * @param string $value
     */
    public function setFilter($name, $value)
    {
        if (is_string($name)) {
            $this->filter[$name] = $value;
        }
    }

    /**
     * validate filter
     */
    public function validFilter()
    {
        if (isset($this->filter['extension'])) {
            $filter = $this->filter['extension'];
            if ($filter != null) {
                if ($this->getExtension() != $filter) {
                    $this->isValid = false;
                }
            }
        }
    }

    /**
     * Return true/false if valid
     * @return bool
     */
    public function isValid()
    {
        return $this->isValid;
    }

    /**
     * added file
     * @param $fileName
     */
    public function addFile($fileName)
    {
        if ($this->checkIfFileExist($fileName)) {
            self::$fileName = $fileName;
        }
    }
}
