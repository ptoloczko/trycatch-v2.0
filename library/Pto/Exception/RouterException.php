<?php
/**
 * Exception for router
 */

namespace Pto\Exception;

/**
 * Class RouterException
 * @package Pto\Exception
 */
class RouterException extends Exception
{
    const ERROR_LOAD_CONTROLLER = 1000;
    const ERROR_LOAD_ACTION = 1001;
}
