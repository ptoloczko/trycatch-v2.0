<?php
/**
 * Setup all exception you want to show in api response
 */

namespace Pto\Exception;

/**
 * Class ApiException
 * @package Pto\Exception
 */
class ApiException extends Exception
{
    const ERROR_WRONG_PARAM = 2000;
    const DATA_NOT_EXIST = 2001;
}
