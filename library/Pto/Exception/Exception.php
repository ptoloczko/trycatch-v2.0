<?php
/**
 * Global Exception for use in project extends default exception
 */

namespace Pto\Exception;

/**
 * Class Exception
 * @package Pto\Exception
 */
class Exception extends \Exception
{
    /**
     * function return error in string format
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: [{$this->message}]\n";
    }
}
