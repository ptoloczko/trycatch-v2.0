<?php
/**
 * Store some request from client
 */

namespace Pto;

/**
 *
 * Its parse request uri, search $_GET,$_POST,$_SERVER
 * set/get params from uri
 *
 * Class Request
 * @package Pto
 */
class Request
{
    /**
     * @const string default application/xhtml+xml
     */
    const DEFAULT_HTTP_ACCEPT = "application/xhtml+xml";

    /**
     * @const string default text/html
     */
    const DEFAULT_HTTP_CONTENT_TYPE = "text/html";

    /**
     * function store method from request
     * @var string
     */
    protected $method;

    /**
     * Parsed params from uri
     * @var array
     */
    protected $params = array();

    /**
     * link url
     * @var string
     */
    protected $uri;

    /**
     * @var array default [_GET,_POST]
     */
    protected $allowParams = ['_GET', '_POST'];

    /**
     * setup url
     * @param null $uri
     */
    public function __construct($uri = null)
    {
        if (null!==$uri) {
            $this->uri = $uri;
        } else {
            $this->setUri();
        }
    }

    /**
     * Save url to variable from REQUEST_URI if $uri null
     * @param null $uri
     * @return $this
     */
    public function setUri($uri = null)
    {
        if (null === $uri) {
            if (isset($_SERVER['REQUEST_URI'])) {
                $uri = $_SERVER['REQUEST_URI'];
            }
        }
        $this->uri = $uri;
        return $this;
    }

    /**
     * Get element by $key from $_GET,$_POST or $_SERVER
     * @param $key
     * @return null
     */
    public function __get($key)
    {
        switch (true) {
            case isset($_GET[$key]):
                return $_GET[$key];
            case isset($_POST[$key]):
                return $_POST[$key];
            case isset($_SERVER[$key]):
                return $_SERVER[$key];
            default:
                return null;
        }
    }

    /**
     * check if $key exist in $_GET,$_POST,$_SERVER or $params[$key]
     * @param $key
     * @return bool
     */
    public function __isset($key)
    {
        switch (true) {
            case isset($this->params[$key]):
                return true;
            case isset($_GET[$key]):
                return true;
            case isset($_POST[$key]):
                return true;
            case isset($_SERVER[$key]):
                return true;
            default:
                return false;
        }
    }

    /**
     * Return find $key in $_GET or $default value
     * @param null $key
     * @param null $default
     * @return null
     */
    public function getQuery($key = null, $default = null)
    {
        if (null === $key) {
            return $_GET;
        }

        return (isset($_GET[$key])) ? $_GET[$key] : $default;
    }




    /**
     * Get all tabs from $allowParams
     * @return array
     */
    public function getAllowParams()
    {
        return $this->allowParams;
    }

    /**
     * Get all params from $params
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Function set params from array key=>value
     * @param array $params
     * @return $this
     */
    public function setParams(array $params)
    {
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                $this->setParam($key, $value);
            }
        }
        return $this;
    }

    /**
     * Function get param from params by $key if not exist set $default
     * @param $key
     * @param null $default
     * @return null/string
     */
    public function getParam($key, $default = null)
    {
        $key = (string) $key;
        return isset($this->params[$key]) ? $this->params[$key] : $default;
    }

    /**
     * Get element from $_POST or set default if not exist
     * @param null $key
     * @param null $default
     * @return null
     */
    public function getPost($key = null, $default = null)
    {
        if (null === $key) {
            return $_POST;
        }

        return (isset($_POST[$key])) ? $_POST[$key] : $default;
    }

    /**
     * Function return url
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set param to $params[$key]
     * @param $key
     * @param $value
     * @return $this
     */
    public function setParam($key, $value)
    {
        $key = (string) $key;
        if ((null === $value) && isset($this->params[$key])) {
            unset($this->params[$key]);
        } elseif (null !== $value) {
            $this->params[$key] = $value;
        }
        return $this;
    }

    /**
     * Get REQUEST_METHOD from Server
     * @return null
     */
    public function getMethod()
    {
        return $this->getServer('REQUEST_METHOD');
    }

    /**
     * Get element from $_SERVER[$key]
     * @param null $key
     * @param null $default
     * @return null
     */
    public function getServer($key = null, $default = null)
    {
        if (null === $key) {
            return $_SERVER;
        }

        return (isset($_SERVER[$key])) ? $_SERVER[$key] : $default;
    }


    /**
     * return element from server HTTP_ACCEPT
     * @return null|string
     */
    public function getAccept()
    {
        if (null === ($accept = $this->getServer('HTTP_ACCEPT'))) {
            $accept = self::DEFAULT_HTTP_ACCEPT;
        }

        return $accept;
    }

    /**
     * Get Content-Type from server
     * @return null|string
     */
    public function getContentType()
    {
        if (null === ($contentType = $this->getServer('CONTENT_TYPE'))) {
            $contentType = self::DEFAULT_HTTP_CONTENT_TYPE;
        }

        return $contentType;
    }
}
