<?php
/**
 * Main Application store request,response, add exception handler and error handler
 */


namespace Pto;

use Pto\Exception\Exception;
use Pto;

/**
 * Class Application
 * @package Pto
 */
class Application
{

    const DEFAULT_ERROR_HANDLER = 'errorHandler';
    const DEFAULT_EXCEPTION_HANDLER = 'errorException';

    /**
     * handler to \Pto\Request
     * @var \Pto\Request
     */
    protected $request;

    /**
     * handler to \Pto\Response
     * @var \Pto\Response
     */
    protected $response;

    /**
     * Set ErrorHandler, ExceptionHandler
     */
    public function __construct()
    {
        $this->setErrorHandler(self::DEFAULT_ERROR_HANDLER);
        $this->setExceptionHandler(self::DEFAULT_EXCEPTION_HANDLER);
    }

    /**
     * Get handler to \Pto\Request
     * @return Request
     */
    public function getRequest()
    {
        if (empty($this->request)) {
            $this->request = new Request();
        }
        return $this->request;
    }

    /**
     * get handler to \Pto\Response
     * @return Response
     */
    public function getResponse()
    {
        if (empty($this->response)) {
            $this->response = new Response();
        }
        return $this->response;
    }

    /**
     * Function set Exception handler
     * @param string $handler
     * @return $this
     */
    public function setExceptionHandler($handler)
    {
        set_exception_handler(array($this, $handler));
        return $this;
    }

    /**
     * Function set Error handler
     * @param string $handler
     * @return $this
     */
    public function setErrorHandler($handler)
    {
        set_error_handler(array($this,$handler));
        return $this;
    }


    /**
     * Custom Exception handler
     * @param Exception $exception
     */
    public function errorException($exception)
    {
        if (Main::isDevelopment()) {
                $this->throwException($exception);
        } else {
            $message = "We are sorry, exist some problem";
            $this->throwException($exception, $message);

            error_log('Unhandled Exception: ' . $exception->getMessage()
                . ' in file ' . $exception->getFile() . ' on line ' . $exception->getLine());
        }
    }

    /**
     * Function return json or html error
     * @param Exception $exception
     * @param string $message
     */
    public function throwException($exception, $message = null)
    {
        $code = 0;

        $type = $this->request->getContentType();

        if ($message===null) {
            $message = $exception->getMessage();
            $code = $exception->getCode();
        }

        switch ($type) {
            case 'application/json':
                $this->response->sendResponse();
                echo json_encode(array('errors' => true, 'message' => $message, 'code' => $code));
                return;
                break;
            default:
                if ($code==0) {
                    echo $message;
                    return;
                }
                print '<pre>';
                print $message . "\n\n";
                print 'In file ' . $exception->getFile() . ' on line ' . $exception->getLine() . "\n\n";
                print $exception->getTraceAsString();
                print '</pre>';
                break;
        }
    }

    /**
     * catch errorHandler
     * @param $errno
     * @param $errstr
     * @param $errfile
     * @param $errline
     * @throws Exception
     */
    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        $errorMessage = ": {$errno} {$errstr}  in {$errfile} on line {$errline}";
        if (Main::isDevelopment()) {
            throw new Exception($errorMessage);
        }
    }
}
