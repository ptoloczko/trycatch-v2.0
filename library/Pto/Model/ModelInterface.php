<?php
/**
 * Interface CRUD
 */

namespace Pto\Model;

/**
 * Its simple crud interface all models of manipulate data must implement this
 * Interface ModelInterface
 * @package Pto\Model
 */
interface ModelInterface
{
    /**
     * for add new data
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * for read data
     * @param $dataId
     * @return mixed
     */
    public function read($dataId);

    /**
     * for update data by $dataId
     * @param $dataId
     * @param array $data
     * @return mixed
     */
    public function update($dataId, array $data);

    /**
     * for delete by $dataId
     * @param $dataId
     * @return mixed
     */
    public function delete($dataId);
}
