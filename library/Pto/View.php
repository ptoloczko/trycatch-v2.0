<?php
/**
 * Class Render view
 */

namespace Pto;

use Pto\View\ViewAbstract;
use Pto\View\ViewInterface;

/**
 * Class View
 * @package Pto
 */
class View extends ViewAbstract implements ViewInterface
{
    /**
     * @const string default '.phtml'
     */
    const VIEW_SUFFIX = '.phtml';

    /**
     * Function render view
     * @uses \Pto\Main::application()
     *
     * @param null $viewPath
     * @internal param string $view path to view
     * @return void
     */
    public function render($viewPath = null)
    {
        Main::application()->getResponse()->sendResponse();

        if ($viewPath === null) {
            $viewPath = self::$viewPath;
        }

        ob_start();

        $filePath = Main::getRootDirectory(). DS .'application'. DS .'views'. DS . strtolower($viewPath) . self::VIEW_SUFFIX;

        if (File::checkIfFileExist($filePath)) {
            require_once $filePath;
        }
        echo ob_get_clean();
    }
}
