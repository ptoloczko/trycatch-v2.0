<?php
/**
 * Interface Configuration
 */

namespace Pto\Configuration;

/**
 * Interface ConfigurationInterface
 * @package Pto\Configuration
 */
interface ConfigurationInterface
{
    /**
     * function need load file
     * @param $fileName
     * @return mixed
     */
    public function loadFile($fileName);

    /**
     * function get all configuration
     * @return array
     */
    public function getData();
}
