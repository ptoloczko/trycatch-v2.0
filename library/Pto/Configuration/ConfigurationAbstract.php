<?php
/**
 * Abstract Configuration
 */
namespace Pto\Configuration;

/**
 * Class ConfigurationAbstract
 * @package Pto\Configuration
 */
abstract class ConfigurationAbstract
{
    /**
     * Function check and return array from $data
     * @param $data
     * @return array
     */
    public function toArray($data)
    {
        $array = array();
        foreach ($data as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }
}
