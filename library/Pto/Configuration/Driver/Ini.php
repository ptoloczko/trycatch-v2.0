<?php
/**
 * Driver to serve configuration *.ini files
 */

namespace Pto\Configuration\Driver;

use Pto;
use Pto\Configuration;
use Pto\Configuration\ConfigurationInterface;
use Pto\Configuration\ConfigurationAbstract;

/**
 * Class Ini
 * @package Pto\Configuration\Driver
 */
class Ini extends ConfigurationAbstract implements ConfigurationInterface
{
    /**
     * Separator for configuration ex. db.name, db.user
     * @var string
     */
    protected $separator = '.';

    /**
     * save all fetch data
     * @var array
     */
    private $data = array();

    /**
     * function store all config from file to $data variable
     * @param string $fileName
     * @throws \Exception
     */
    public function __construct($fileName)
    {

        if (empty($fileName)) {
            throw new \Exception('Filename is not set');
        }

        $returnArray = $this->loadFile($fileName);

        foreach ($returnArray as $name => $value) {
                $this->data[$name] = $value;
        }
    }

    /**
     * return all save data from file
     */
    public function getData()
    {
        return $this->toArray($this->data);
    }

    /**
     * Function parse_ini_file
     * @param $fileName
     * @return array
     */
    public function loadFile($fileName)
    {
        $iniArray = parse_ini_file($fileName, true);
        return $iniArray;
    }
}
