<?php
/**
 * Main class to routing connect controller, action and view
 */

namespace Pto;

use Pto\Exception\RouterException;
use Pto\Controller\ControllerAbstract;

/**
 * Class Router
 * @package Pto
 */
class Router
{

    const DEFAULT_CONTROLLER = 'index';
    const DEFAULT_ACTION = 'index';

    /**
     * Name of controller or handler to \Pto\Controller
     * @var ControllerAbstract|string
     */
    protected $controller = self::DEFAULT_CONTROLLER;

    /**
     * @var string string
     */
    protected $action = self::DEFAULT_ACTION;

    /**
     * handler to ControllerAbstract
     * @var Controller\ControllerAbstract
     */
    protected $view;

    /**
     * string $url
     * @var string
     */
    protected $url;

    /**
     * @var string default Controller
     */
    protected $controllerSuffix = 'Controller';

    /**
     * array of parts send by client
     * @var array
     */
    private static $parts = array();

    /**
     * setUp some elements and parseUrl, dispatch
     */
    public function __construct()
    {
        $this->url = Main::application()->getRequest()->getUri();
        Main::application()->getResponse()->setContentType(Main::application()->getRequest()->getContentType());
        $this->parseUrl();
        $this->dispatch();
    }

    /**
     * dispatch mvc
     */
    public function dispatch()
    {
        call_user_func_array(array($this->controller, $this->action), Main::application()->getRequest()->getParams());

        $this->controller->view->render();
    }

    /**
     * Function parse route controller/action and store params
     */
    private function parseUrl()
    {
        $parts = parse_url($this->url);

        self::$parts = explode("/", trim($parts['path'], "/"));

        $params = array();

        if (isset($parts['query'])) {
            parse_str($parts['query'], $params);
        }

        $body = file_get_contents("php://input");
        $body_params = json_decode($body);

        if ($body_params) {
            foreach ($body_params as $param_name => $param_value) {
                $params[$param_name] = $param_value;
            }
        }


        $size = sizeof(self::$parts);

        if ($size > 0) {
            $controllerName = self::$parts[0];

            if (empty($controllerName)) {
                $controllerName = self::DEFAULT_CONTROLLER;
                $this->action = self::DEFAULT_ACTION;
            }

            self::loadController($controllerName);

            $method = strtolower(Main::application()->getRequest()->getMethod());

            if (isset(self::$parts[0])) {
                if ($method != 'get') {
                    self::loadAction($method);
                } else {
                    self::loadAction(self::$parts[0]);
                }
            } else {
                if ($method != 'get') {
                    self::loadAction($method);
                }
            }

            $this->loadViewFile($controllerName, $this->action);
        }

        Main::application()->getRequest()->setParams($params);
    }

    /**
     * Function load and run Class send in param name
     * @param string $controllerName name of class controller
     * @throws RouterException
     */
    protected function loadController($controllerName)
    {
        $controllerName = ucfirst($controllerName);

        if (Autoload::isLoaded($controllerName)) {
            return;
        }

        $controllerPath = "application/controllers/{$controllerName}{$this->controllerSuffix}.php";

        if (!Autoload::loadFile($controllerPath)) {
            throw new RouterException(
                sprintf('Error load controller path "%s"', $controllerPath),
                RouterException::ERROR_LOAD_CONTROLLER
            );
        }

        $this->controller = $controllerName . $this->controllerSuffix;
        array_shift(self::$parts);
        $this->controller = new $this->controller;
    }


    /**
     * Check if file view exist and connect to viewPath
     * @param $controllerName
     * @param $actionName
     */
    private function loadViewFile($controllerName, $actionName)
    {
        $viewPath = Main::getRootDirectory(). DS . 'application'. DS .'views'.
            DS .strtolower($controllerName). DS . strtolower($actionName).View::VIEW_SUFFIX;

        if (File::checkIfFileExist($viewPath)) {
            View::$viewPath = $controllerName . DS . $actionName;
        }
    }

    /**
     * Function check if method in class exist if not try default $method
     * @param $actionName - name of method in class
     * @throws \Exception
     */
    protected function loadAction($actionName)
    {
        if (method_exists($this->controller, $actionName)) {
            $this->action = $actionName;
            array_shift(self::$parts);
        } elseif (method_exists($this->controller, self::DEFAULT_ACTION)) {
            $this->action = self::DEFAULT_ACTION;
        } else {
            throw new RouterException(
                sprintf('Error load action "%s"', $this->action),
                RouterException::ERROR_LOAD_ACTION
            );
        }
    }
}
