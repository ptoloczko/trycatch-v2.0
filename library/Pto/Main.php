<?php
/**
 * Main final function you need start it Main::run()
 */

namespace Pto;

use Pto;
use Pto\Exception\Exception;

/**
 * Global final function I set final you can't extends this function
 *
 * store some global elements
 *
 * Class Main
 * @package Pto
 */
final class Main
{

    /**
     * @var array Global Registry collection
     */
    static private $registry = array();

    /**
     * Application model
     * @var Pto\Router
     */
    static private $router;


    /**
     * @var string type of configuration file defaults ini
     */
    static private $type='ini';

    /**
     * @var array() options
     */
    static private $options;

    /**
     * @var string environment of application
     */
    static private $env;

    /**
     * url of Request
     * @var string
     */
    static private $uri;

    /**
     * Configuration Model
     * @var Pto\Configuration
     */
    static private $config;

    /**
     * Root directory
     * @var string
     */
    static private $rootDir;

    /**
     * handler to \Pto\Application
     * @var \Pto\Application
     */
    static private $application;

    /**
     * Main function set environment of application and file_type where setup configuration
     * @param string $environment set environment
     * @param string $type - type of file configs (ini,php,...)
     * @param string $fileName - link to file name ex. APPLICATION_ROOT.'/configs/config.ini';
     * @throws \Exception
     */
    public static function run($environment, $type, $fileName)
    {
        try {
            self::$env = $environment;
            self::$type = $type;

            self::setConfigModel();
            self::setRootDirectory(APPLICATION_ROOT);

            //register custom autoloader function
            Autoload::getInstance()->register();

            if (null !== $fileName && is_string($fileName)) {
                    self::$options = self::getConfig()->loadConfiguration($fileName);
            }
            self::$application = new Application();
            self::$uri = self::$application->getRequest()->getUri();
            self::$router = new Router();
        } catch (Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * Get url
     * @return string
     */
    public static function getUri()
    {
        return self::$uri;
    }

    /**
     * return handle to \Pto\Application
     * @return Application
     */
    public static function application()
    {
        if (null === self::$application) {
            self::$application = new Application();
        }
        return self::$application;
    }

    /**
     * Check Environment if set for developer
     * @return bool
     */
    public static function isDevelopment()
    {
        return (self::$env=='development') ? true:false;
    }

    /**
     * add some element to $registry
     * @param $name
     * @param $value
     */
    protected static function addRegistry($name, $value)
    {
        self::$registry[$name] = $value;
    }

    /**
     * Get element from registry
     * @param $name
     * @return null
     */
    protected static function getRegistry($name)
    {
        return (isset(self::$registry[$name])) ? self::$registry[$name]: null;
    }

    /**
     * set ROOT directory
     * @param $path
     */
    protected static function setRootDirectory($path)
    {
        self::$rootDir = $path;
    }

    /**
     * Get ROOT directory
     * @return string
     */
    public static function getRootDirectory()
    {
        return self::$rootDir;
    }

    /**
     * set handle to \Pto\Configuration
     */
    protected static function setConfigModel()
    {
        self::$config = new Configuration(self::$type);
    }

    /**
     * Get handle to Configuration
     * @return Configuration
     */
    public static function getConfig()
    {
        return self::$config;
    }

    /**
     * Return option by $key
     * @param $key
     * @return mixed string/false
     */
    public static function getOption($key)
    {
        if (!is_string($key)) {
            return false;
        }

        return isset(self::$options[$key]) ? self::$options[$key] : false;
    }
}
