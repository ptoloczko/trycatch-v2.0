<?php
/**
 * Abstract ApiController for RESTful service
 * If you add this controller need use all this function
 */

namespace Pto\Controller;

/**
 * Class ApiControllerAbstract
 * @package Pto\Controller
 */
abstract class ApiControllerAbstract extends ControllerAbstract
{
    /**
     * index for get data in REST
     * @return mixed
     */
    abstract public function index();

    /**
     * post for create new data in REST
     * @return mixed
     */
    abstract public function post();

    /**
     * get no not used automatic set to index
     * @return mixed
     */
    abstract public function get();

    /**
     * delete data in REST
     * @return mixed
     */
    abstract public function delete();

    /**
     * PUT edit data in REST
     * @return mixed
     */
    abstract public function put();
}
