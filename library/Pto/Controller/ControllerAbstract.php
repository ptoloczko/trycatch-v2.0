<?php
/**
 * Abstract function from Controller usage
 */

namespace Pto\Controller;

use Pto\Main;
use Pto\View;

/**
 * Class ControllerAbstract
 * @package Pto\Controller
 */
abstract class ControllerAbstract
{
    /**
     * Handle to \Pto\View
     * @var \Pto\View\ViewInterface
     */
    public $view;

    /**
     * init View
     */
    public function __construct()
    {
        $this->initView();
    }

    /**
     * Function set view
     * @param $view
     * @return View\ViewInterface
     */
    public function initView($view = null)
    {
        if (isset($this->view) && ($this->view instanceof View\ViewInterface)) {
            return $this->view;
        }
        if ($view === null) {
            $this->view = new View($view);
        } else {
            $this->view = new View();
        }
        return $this;
    }

    /**
     * Function run $this->view->render() from controller
     */
    public function render()
    {
        /** @var View $this */
        if (isset($this->view)) {
            $this->view->render();
        }
    }

    /**
     * function return getRequest();
     * @return \Pto\Request
     */
    public function getRequest()
    {
        return Main::application()->getRequest();
    }
}
