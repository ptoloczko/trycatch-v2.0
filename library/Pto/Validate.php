<?php
/**
 * Class Validate variables
 */

namespace Pto;

use Pto\Exception\ApiException;

/**
 * Class Validate
 * @package Pto
 */
class Validate
{
    /**
     * Function use trim and strip_tags for variable
     * @param mixed $variable
     * @return string
     */
    public static function cleanInput($variable)
    {
        return trim(strip_tags($variable));
    }

    /**
     * Function validate to integer
     * @param $variable
     * @param string $variableName
     * @return int
     * @throws ApiException
     */
    public static function validateInt($variable, $variableName = '')
    {
        $variable = self::cleanInput($variable);
        if (false !== ($ret = filter_var($variable, FILTER_VALIDATE_INT))) {
            return $ret;
        } else {
            throw new ApiException($variableName.' must be a number', ApiException::ERROR_WRONG_PARAM);
        }
    }

    /**
     * Function validate to string
     * @param $variable
     * @param string $variableName name of variable if throw Exception
     * @return string
     * @throws ApiException
     */
    public static function validateString($variable, $variableName = '')
    {
        $variable = self::cleanInput($variable);
        if (!is_string($variable)) {
            throw new ApiException($variableName.' must be a string', ApiException::ERROR_WRONG_PARAM);
        } else {
            return $variable;
        }
    }
}
