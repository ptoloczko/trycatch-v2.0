<?php
/**
 * Interface of View
 */

namespace Pto\View;

/**
 * Interface ViewInterface
 * @package Pto\View
 */
interface ViewInterface
{
    /**
     * Render view file
     * @param null|string $viewPath path to view file
     * @return mixed
     */
    public function render($viewPath = null);
}
