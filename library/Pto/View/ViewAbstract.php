<?php
/**
 * Abstract of View
 */

namespace Pto\View;

/**
 * Class ViewAbstract
 * @package Pto\View
 */
abstract class ViewAbstract
{
    /**
     * store variables you can use in .phtml view file ex. $this->user
     * @var array
     */
    public static $variables = array();

    /**
     * path to view file
     * @var string
     */
    public static $viewPath;

    /**
     * magic method to store variable in $variables
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        self::$variables[$name] = $value;
    }

    /**
     * magic method to get variable from $variables
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        return (isset($name) && isset(self::$variables[$name])) ? self::$variables[$name] : null;
    }
}
