<?php
/**
 * Class for autoloader class
 */


namespace Pto;

use Pto;

/**
 * Function to autoload class from Application
 * Class Autoload
 * @package Pto
 */
class Autoload
{

    /**
     * Instance
     * @var $this
     */
    protected static $instance;

    /**
     * store resources to autoload class ex. ['Application_Model' => 'application/models']
     * @var array
     */
    protected $registerResources = array('Application_Model' => 'application/models');

    /**
     * public static method to get instance of class
     * @return Autoload
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * block clone
     */
    public function __clone()
    {
    }

    /**
     * Register class and function autoload
     */
    public static function register()
    {
        spl_autoload_register(array(self::getInstance(), 'autoload'));
    }


    /**
     *
     * Try load class ex. Application_Model_Users
     *
     * @param string $className name of class
     */
    public function autoload($className)
    {
        if (self::isLoaded($className)) {
            return;
        }

        $file = $this->parseFileClass($className);

        if ($file) {
            self::loadFile($file);
        }
    }

    /**
     * Load file from path
     * @param $fileName
     * @return bool true/false
     */
    public static function loadFile($fileName)
    {
        $filePath = Main::getRootDirectory(). DS . $fileName;

        if (is_readable($filePath)) {
            require_once $filePath;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if class is loaded
     * @param string $className name of class ex. Application_Model_Users
     * @return bool true/false
     */
    public static function isLoaded($className)
    {
        if (class_exists($className, false) || interface_exists($className, false)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * parse ClassName to filePath, check if class [name => path] register in $_register_resources
     *
     * ex. Application_Models_Users parse to [root path]/application/model/Users.ph
     *
     * Return parse url file
     *
     * @param string $className name of class
     * @return bool|string
     */
    public function parseFileClass($className)
    {
        $elements = explode('_', $className);

        $filePath =  $findPath = '';

        do {
            $tmpElements = array_shift($elements);
            $filePath .= empty($filePath) ? $tmpElements : '_' . $tmpElements;
            if (isset($this->registerResources[$filePath])) {
                $findPath = $filePath;
            }
        } while (count($elements));

        if (trim($findPath)==='') {
            return false;
        }

        $final = substr($className, strlen($findPath) + 1);
        $path = $this->registerResources[$findPath];
        $filePath = $path . '/' . str_replace('_', '/', $final) . '.php';

        return $filePath;
    }
}
